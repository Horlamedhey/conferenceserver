const MongoClient = require("mongodb").MongoClient;
module.exports = (function () {
  return MongoClient.connect(
    "mongodb://127.0.0.1:27017/conference?readPreference=primary&appname=MongoDB%20Compass&ssl=false",
    { useUnifiedTopology: true }
  )
    .then((client) => client.db("conference"))
    .then((db) => {
      return {
        usersCollection: db.collection("users"),
        chatsCollection: db.collection("chats"),
      };
    });
})();
